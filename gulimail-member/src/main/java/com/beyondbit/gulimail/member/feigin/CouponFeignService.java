package com.beyondbit.gulimail.member.feigin;

import com.beyondbit.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author lfx
 * @version 1.0
 * @date 2020/6/16 14:50
 */
@FeignClient("gulimail-coupon")
public interface CouponFeignService {
    @RequestMapping("coupon/coupon/member/list")
    public R membercoupons();
}
