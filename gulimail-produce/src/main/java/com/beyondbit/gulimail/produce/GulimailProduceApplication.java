package com.beyondbit.gulimail.produce;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@MapperScan("com.beyondbit.gulimail.produce.dao")
@SpringBootApplication
public class GulimailProduceApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimailProduceApplication.class, args);
    }

}
