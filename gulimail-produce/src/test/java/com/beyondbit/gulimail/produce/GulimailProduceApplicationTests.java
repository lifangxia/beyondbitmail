package com.beyondbit.gulimail.produce;

import com.beyondbit.gulimail.produce.entity.BrandEntity;
import com.beyondbit.gulimail.produce.service.BrandService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class GulimailProduceApplicationTests {

    @Autowired
    BrandService brandService;
    @Test
   public void contextLoads() {
        BrandEntity entity=new BrandEntity();
        entity.setBrandId(1L);
        entity.setDescript("华为");
//        entity.setName("华为");
//        brandService.save(entity);
//        System.out.println("保存成功");

        brandService.updateById(entity);
    }

}
